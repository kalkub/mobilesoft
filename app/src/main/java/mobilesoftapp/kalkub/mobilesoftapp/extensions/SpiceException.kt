package mylaw.app.extensions

import com.octo.android.robospice.exception.NetworkException
import com.octo.android.robospice.persistence.exception.SpiceException
import retrofit.RetrofitError

/**
 * Created by kristyna on 1/26/18.
 */

fun SpiceException.getStatusCode(): Int {
    if (this is NetworkException)
        if (this.cause is RetrofitError)
            return (this.cause as RetrofitError).response.status

    return 500
}