package mylaw.app.extensions

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import mobilesoftapp.kalkub.mobilesoftapp.R


fun AppCompatActivity.showFragment(idContainer: Int, baseFragment: Fragment, backStack: Boolean) {
    val transaction = supportFragmentManager.beginTransaction()
    transaction.replace(idContainer, baseFragment, baseFragment.tag)
    if (backStack) {
        transaction.addToBackStack(baseFragment.tag)
    }

    transaction.commit()
}

fun AppCompatActivity.showFragment(baseFragment: Fragment, backStack: Boolean) {
    showFragment(R.id.container, baseFragment, backStack)
}