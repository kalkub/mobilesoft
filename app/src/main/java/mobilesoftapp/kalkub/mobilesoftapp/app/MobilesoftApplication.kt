package mobilesoftapp.kalkub.mobilesoftapp.app

import android.app.Application
import io.realm.Realm

class MobilesoftApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
    }
}