package mobilesoftapp.kalkub.mobilesoftapp.ui.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.widget.Toast
import mobilesoftapp.kalkub.mobilesoftapp.R
import mobilesoftapp.kalkub.mobilesoftapp.ui.base.BaseActivity
import mobilesoftapp.kalkub.mobilesoftapp.ui.create.EmployeeCreateFragment
import mobilesoftapp.kalkub.mobilesoftapp.ui.create.EmployeeCreateFragment_
import mobilesoftapp.kalkub.mobilesoftapp.ui.detail.EmployeeDetailFragment
import mylaw.app.extensions.showFragment
import org.androidannotations.annotations.*

@EActivity(R.layout.activity_main)
open class MainActivity : BaseActivity() {

    @ViewById
    lateinit var toolbar: Toolbar

    @AfterViews
    fun init() {
        setSupportActionBar(toolbar)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showFragment(EmployeesListFragment_.builder().build(), false)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var fragment = supportFragmentManager.findFragmentById(R.id.container)
        if(fragment !is EmployeeDetailFragment && fragment !is EmployeeCreateFragment)
            menuInflater.inflate(R.menu.menu_add, menu)

        return true
    }

    @OptionsItem(R.id.add)
    fun add() {
        showFragment(EmployeeCreateFragment_.builder().build(), true)
    }
}
