package mobilesoftapp.kalkub.mobilesoftapp.ui.base

import android.support.v7.app.AppCompatActivity
import com.octo.android.robospice.SpiceManager
import mylaw.app.io.net.service.RequestService_

open class BaseActivity : AppCompatActivity() {

    val manager = SpiceManager(RequestService_::class.java)

    override fun onStart() {
        super.onStart()
        if(!manager.isStarted)
            manager.start(this)
    }

    override fun onStop() {
        super.onStop()

        if (manager.isStarted)
            manager.shouldStop()
    }

}