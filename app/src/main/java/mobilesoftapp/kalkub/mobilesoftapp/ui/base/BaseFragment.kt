package mobilesoftapp.kalkub.mobilesoftapp.ui.base

import android.content.Context
import android.support.v4.app.Fragment
import mobilesoftapp.kalkub.mobilesoftapp.ui.main.MainActivity

open class BaseFragment : Fragment() {

    lateinit var baseActivity : BaseActivity

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        baseActivity = context as BaseActivity
    }

}