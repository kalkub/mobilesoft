package mobilesoftapp.kalkub.mobilesoftapp.ui.main.adapter

import android.support.v7.widget.RecyclerView
import mobilesoftapp.kalkub.mobilesoftapp.ui.main.holder.EmployeeHolder
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.view.LayoutInflater
import android.view.ViewGroup
import mobilesoftapp.kalkub.mobilesoftapp.R
import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import mobilesoftapp.kalkub.mobilesoftapp.ui.base.BaseActivity


class EmployeesListAdapter : RecyclerView.Adapter<EmployeeHolder>() {

    var list: ArrayList<Employee> = arrayListOf()
    lateinit var base: BaseActivity

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): EmployeeHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding : ViewDataBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_employee, parent, false)
        return EmployeeHolder(itemBinding, base)
    }

    override fun onBindViewHolder(holder: EmployeeHolder,
                                  position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}