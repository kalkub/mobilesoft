package mobilesoftapp.kalkub.mobilesoftapp.ui.main.viewmodel

import android.content.Context
import android.databinding.BindingAdapter
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ImageView
import com.squareup.picasso.Picasso
import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import mobilesoftapp.kalkub.mobilesoftapp.ui.base.BaseActivity
import mobilesoftapp.kalkub.mobilesoftapp.ui.detail.EmployeeDetailFragment_
import mobilesoftapp.kalkub.mobilesoftapp.ui.main.adapter.EmployeesListAdapter
import mylaw.app.extensions.showFragment
import mylaw.app.io.net.service.RequestService
import java.text.NumberFormat

class EmployeeViewModel(var employee: Employee, var base: BaseActivity) {

    private val numberFormat = NumberFormat.getNumberInstance()

    fun getSalary(): String {
        if(employee == null || employee.salary == 0) {
            return "-"
        } else {
            return numberFormat.format(employee.salary) + " Kč"
        }
    }

    fun openDetail(view: View) {
        base.showFragment(EmployeeDetailFragment_.builder().employeeID("${employee.employeeID}").build(), true)
    }

    companion object {
        @JvmStatic
        @BindingAdapter(value = "src")
        fun setImage(view: ImageView, src: String?) {
            view.setImageBitmap(null)
            if(src.isNullOrEmpty()) {
                view.setBackgroundColor(Color.parseColor("#b1b1b1"))
            } else {
                Picasso.with(view.context)
                        .load(RequestService.APIData.BASE_IMAGE + src)
                        .fit()
                        .centerCrop()
                        .into(view)

            }
        }
    }

}