package mobilesoftapp.kalkub.mobilesoftapp.ui.create.viewmodel

import android.content.Context
import android.databinding.BindingAdapter
import android.databinding.ObservableField
import android.graphics.Color
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.octo.android.robospice.persistence.exception.SpiceException
import com.octo.android.robospice.request.listener.RequestListener
import com.squareup.picasso.Picasso
import com.vicpin.krealmextensions.save
import mobilesoftapp.kalkub.mobilesoftapp.R
import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import mobilesoftapp.kalkub.mobilesoftapp.io.net.request.CreateEmploeeRequest
import mobilesoftapp.kalkub.mobilesoftapp.ui.base.BaseFragment
import mobilesoftapp.kalkub.mobilesoftapp.ui.create.EmployeeCreateFragment
import mylaw.app.io.net.service.RequestService
import java.io.File
import mobilesoftapp.kalkub.mobilesoftapp.R.id.view
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager


class EmployeeCreateViewModel {

    var fragment: BaseFragment
    var name: ObservableField<String>
    var address: ObservableField<String>
    var department: ObservableField<String>
    var salary: ObservableField<String>
    var photo: ObservableField<String>
    var photoVisible: ObservableField<Boolean>

    constructor(fragment: BaseFragment) {
        this.fragment = fragment
        this.name = ObservableField()
        this.address = ObservableField()
        this.department = ObservableField()
        this.salary = ObservableField()
        this.photo = ObservableField()
        this.photoVisible = ObservableField(true)
    }

    fun openCamera(v: View) {
        (fragment as EmployeeCreateFragment).openCamera()
    }

    fun save(view: View) {

        when {
            name.get().isNullOrEmpty() -> {
                Toast.makeText(view.context, R.string.error_empty_name, Toast.LENGTH_LONG).show()
                return
            }
            department.get().isNullOrEmpty() -> {
                Toast.makeText(view.context, R.string.error_empty_department, Toast.LENGTH_LONG).show()
                return
            }
        }

        var employee = Employee()
        employee.name = name.get()
        employee.address = address.get()
        employee.department = department.get()
        if(!salary.get().isNullOrEmpty()) {
            try {
                employee.salary = salary.get().toInt()
            } catch (e: Exception) {
            }
        }
        view.isEnabled = false

        fragment.baseActivity.manager.execute(CreateEmploeeRequest(employee, if(photo.get().isNullOrEmpty()) null else File(photo.get())),
                object : RequestListener<Employee> {
                    override fun onRequestSuccess(result: Employee?) {
                        view.isEnabled = true
                        result?.save()
                        fragment.baseActivity.onBackPressed()
                        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(view.windowToken, 0)
                        Toast.makeText(view.context, R.string.create_employee_created, Toast.LENGTH_LONG).show()
                    }

                    override fun onRequestFailure(spiceException: SpiceException?) {
                        Toast.makeText(view.context, R.string.error_toast, Toast.LENGTH_LONG).show()
                        view.isEnabled = true
                    }
        })
    }

    companion object {
        @JvmStatic
        @BindingAdapter(value = "load")
        fun loadImage(view: ImageView, src: String?) {
            if(src.isNullOrEmpty()) {
                view.setBackgroundColor(Color.parseColor("#b1b1b1"))
                view.setImageBitmap(null)
            } else {
                view.setImageBitmap(null)
                Picasso.with(view.context)
                        .load(File(src))
                        .into(view)

            }
        }
    }


}