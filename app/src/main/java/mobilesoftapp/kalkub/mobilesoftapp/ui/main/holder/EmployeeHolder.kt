package mobilesoftapp.kalkub.mobilesoftapp.ui.main.holder

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View
import mobilesoftapp.kalkub.mobilesoftapp.BR
import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import mobilesoftapp.kalkub.mobilesoftapp.ui.base.BaseActivity
import mobilesoftapp.kalkub.mobilesoftapp.ui.main.viewmodel.EmployeeViewModel
import org.androidannotations.annotations.DataBound

class EmployeeHolder(var binding: ViewDataBinding, var base: BaseActivity) : RecyclerView.ViewHolder(binding.root) {

    fun bind(employee: Employee) {
        binding.setVariable(BR.data, EmployeeViewModel(employee, base))
        binding.executePendingBindings()
    }
}