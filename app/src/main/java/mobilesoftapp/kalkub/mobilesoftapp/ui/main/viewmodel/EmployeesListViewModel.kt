package mobilesoftapp.kalkub.mobilesoftapp.ui.main.viewmodel

import android.databinding.BindingAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import mobilesoftapp.kalkub.mobilesoftapp.ui.base.BaseActivity
import mobilesoftapp.kalkub.mobilesoftapp.ui.main.adapter.EmployeesListAdapter

class EmployeesListViewModel(var items: ArrayList<Employee>?) {

    companion object {
        @JvmStatic
        @BindingAdapter(value = "items")
        fun setRecycler(view: RecyclerView, items: ArrayList<Employee>?) {
            if(items != null) {
                val adapter = EmployeesListAdapter()
                adapter.base = view.context as BaseActivity
                adapter.list = items!!
                view.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
                view.adapter = adapter
            }
        }
    }

}