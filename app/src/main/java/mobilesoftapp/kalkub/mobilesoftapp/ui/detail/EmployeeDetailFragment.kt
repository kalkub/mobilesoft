package mobilesoftapp.kalkub.mobilesoftapp.ui.detail

import android.widget.Toast
import com.octo.android.robospice.persistence.exception.SpiceException
import com.octo.android.robospice.request.listener.RequestListener
import com.vicpin.krealmextensions.*
import mobilesoftapp.kalkub.mobilesoftapp.R
import mobilesoftapp.kalkub.mobilesoftapp.databinding.FragmentEmployeeDetailBinding
import mobilesoftapp.kalkub.mobilesoftapp.extensions.isConnectedToInternet
import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import mobilesoftapp.kalkub.mobilesoftapp.io.net.request.GetEmployeeRequest
import mobilesoftapp.kalkub.mobilesoftapp.ui.base.BaseFragment
import mobilesoftapp.kalkub.mobilesoftapp.ui.main.viewmodel.EmployeeViewModel
import org.androidannotations.annotations.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

@DataBound
@EFragment(R.layout.fragment_employee_detail)
open class EmployeeDetailFragment : BaseFragment(), RequestListener<Employee> {

    @FragmentArg
    lateinit var employeeID: String

    //@BindingObject
    //lateinit var binding: FragmentEmployeeDetailBinding

    @AfterViews fun init() {
        baseActivity.invalidateOptionsMenu()
        baseActivity.supportActionBar?.title = getString(R.string.employee_detail_title)
        if(baseActivity.isConnectedToInternet()) {
            baseActivity.manager.execute(GetEmployeeRequest(employeeID.toLong()), this)
        } else {
            doAsync {
                var employee = Employee().queryFirst {
                    equalTo("employeeID", employeeID.toLong())
                }
                if(employee != null) {
                    uiThread {
                        showData(employee)
                    }
                }
            }
        }
    }

    override fun onRequestSuccess(result: Employee) {
        doAsync {
            result.save()
        }
        showData(result)
    }

    override fun onRequestFailure(spiceException: SpiceException?) {
        Toast.makeText(baseActivity, R.string.error_toast, Toast.LENGTH_LONG).show()
    }

    fun showData(employee: Employee) {
        //binding.data = EmployeeViewModel(employee, baseActivity)
        //binding.executePendingBindings()
    }
}