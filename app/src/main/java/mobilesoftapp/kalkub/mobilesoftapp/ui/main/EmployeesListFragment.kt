package mobilesoftapp.kalkub.mobilesoftapp.ui.main

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.octo.android.robospice.persistence.exception.SpiceException
import com.octo.android.robospice.request.listener.RequestListener
import com.vicpin.krealmextensions.queryAll
import com.vicpin.krealmextensions.saveAll
import mobilesoftapp.kalkub.mobilesoftapp.R
import mobilesoftapp.kalkub.mobilesoftapp.databinding.FragmentEmployeesListBinding
import mobilesoftapp.kalkub.mobilesoftapp.extensions.isConnectedToInternet
import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import mobilesoftapp.kalkub.mobilesoftapp.io.net.request.GetEmploeesRequest
import mobilesoftapp.kalkub.mobilesoftapp.ui.base.BaseFragment
import mobilesoftapp.kalkub.mobilesoftapp.ui.main.viewmodel.EmployeesListViewModel
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.BindingObject
import org.androidannotations.annotations.DataBound
import org.androidannotations.annotations.EFragment
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

@DataBound
@EFragment(R.layout.fragment_employees_list)
open class EmployeesListFragment : BaseFragment(), RequestListener<Any> {

    //@BindingObject
    //lateinit var binding: FragmentEmployeesListBinding

    @AfterViews fun init() {
        baseActivity.invalidateOptionsMenu()
        baseActivity.supportActionBar?.title = getString(R.string.employees_list_title)
        if(baseActivity.isConnectedToInternet()) {
            baseActivity.manager.execute(GetEmploeesRequest(), this)
        } else {
            doAsync {
                var data = Employee().queryAll() as ArrayList<Employee>
                uiThread {
                    showData(data)
                }
            }
        }
    }

    override fun onRequestSuccess(result: Any) {
        var array = result as ArrayList<Employee>
        showData(array)
        doAsync {
            array.saveAll()
        }
    }

    override fun onRequestFailure(spiceException: SpiceException?) {
        Toast.makeText(baseActivity, R.string.error_toast, Toast.LENGTH_LONG).show()
    }

    fun showData(list: ArrayList<Employee>) {
        //binding.data = EmployeesListViewModel(list)
        //binding.executePendingBindings()
    }
}