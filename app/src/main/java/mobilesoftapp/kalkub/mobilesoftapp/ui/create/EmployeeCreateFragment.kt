package mobilesoftapp.kalkub.mobilesoftapp.ui.create

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import mobilesoftapp.kalkub.mobilesoftapp.R
import mobilesoftapp.kalkub.mobilesoftapp.databinding.FragmentEmployeeCreateBinding
import mobilesoftapp.kalkub.mobilesoftapp.ui.base.BaseFragment
import mobilesoftapp.kalkub.mobilesoftapp.ui.create.viewmodel.EmployeeCreateViewModel
import android.os.Environment.DIRECTORY_PICTURES
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.util.Log
import org.androidannotations.annotations.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


@DataBound
@EFragment(R.layout.fragment_employee_create)
open class EmployeeCreateFragment: BaseFragment() {

    companion object {
        const val REQUEST_TAKE_PHOTO = 1
    }

    open var mCurrentPhotoPath: String? = null

    lateinit var viewModel: EmployeeCreateViewModel

    //@BindingObject
    //lateinit var binding: FragmentEmployeeCreateBinding

    @AfterViews fun init() {
        baseActivity.invalidateOptionsMenu()
        baseActivity.supportActionBar?.title = getString(R.string.employee_create_title)
        viewModel = EmployeeCreateViewModel(this)
        //binding.data = viewModel
        //binding.executePendingBindings()
    }


    fun openCamera() {
        var takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(baseActivity.packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex : IOException) {
            }
            if (photoFile != null) {
                var photoURI = FileProvider.getUriForFile(baseActivity,
                "mobilesoftapp.kalkub.mobilesoftapp", photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == EmployeeCreateFragment.REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            viewModel.photo.set(mCurrentPhotoPath!!)
            viewModel.photoVisible.set(false)
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = baseActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        )
        mCurrentPhotoPath = image.getAbsolutePath()
        return image
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString("photo", mCurrentPhotoPath)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        mCurrentPhotoPath = savedInstanceState?.getString("photo")
    }
}
