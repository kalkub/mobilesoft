package mobilesoftapp.kalkub.mobilesoftapp.io.net.request

import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import mylaw.app.io.net.requests.BaseRequest

class GetEmployeeRequest(var id: Long) : BaseRequest<Employee>(Employee::class.java) {

    override fun loadData(): Employee {
        return service.getEmployee(id)
    }

}