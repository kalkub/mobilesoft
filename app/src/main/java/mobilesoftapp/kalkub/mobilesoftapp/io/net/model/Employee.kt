package mobilesoftapp.kalkub.mobilesoftapp.io.net.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import org.parceler.Parcel

@Parcel
open class Employee : RealmObject() {

    @SerializedName("id")
    var employeeID: Long? = 0
    var name: String = ""
    var department: String = ""
    var address: String = ""
    var salary: Int? = null
    var photoUrl: String? = null

}