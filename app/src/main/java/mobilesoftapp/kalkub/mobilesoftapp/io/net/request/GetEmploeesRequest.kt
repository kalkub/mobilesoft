package mobilesoftapp.kalkub.mobilesoftapp.io.net.request

import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import mylaw.app.io.net.requests.BaseRequest

class GetEmploeesRequest : BaseRequest<Any>(Any::class.java) {

    override fun loadData(): ArrayList<Employee> {
        return service.getEmployees()
    }


}