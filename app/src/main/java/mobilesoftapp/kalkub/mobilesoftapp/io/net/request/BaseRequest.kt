package mylaw.app.io.net.requests

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest
import mylaw.app.io.net.service.ApiService
import retrofit.RetrofitError

abstract class BaseRequest<T>(clazz : Class<T>) : RetrofitSpiceRequest<T, ApiService>(clazz, ApiService::class.java) {

    @Throws(Exception::class)
    override fun loadDataFromNetwork(): T {
        try {
            onPreExecute()
            val result = loadData()
            onPostExecute(result)
            return result
        } catch (error: RetrofitError) {
            val response = error.response
            if (response != null) {
                if (!onError(error)) {
                    val status = response.status
                    if (status in 400..501) {
                        // Disable retry
                        retryPolicy = null
                    }
                }
            }
            throw error
        }
    }

    /**
     * Allows performing operation before original request is made.
     */
    @Throws(Exception::class)
    private fun onPreExecute() {}

    /**
     * Allows performing operation after original request is made.
     *
     * @param result
     */
    @Throws(Exception::class)
    private fun onPostExecute(result: T) {}

    /**
     * Allows custom handling of error
     *
     * @return false if error is consumed, when true is returned default error policy is applied
     */
    @Throws(Exception::class)
    private fun onError(error: RetrofitError): Boolean {
        return false
    }

    @Throws(Exception::class)
    protected abstract fun loadData(): T

}