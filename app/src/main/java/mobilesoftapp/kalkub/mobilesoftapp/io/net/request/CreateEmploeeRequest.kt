package mobilesoftapp.kalkub.mobilesoftapp.io.net.request

import com.google.gson.Gson
import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import mylaw.app.io.net.requests.BaseRequest
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class CreateEmploeeRequest(var employee: Employee, var photo: File?) : BaseRequest<Employee>(Employee::class.java) {


    override fun loadData(): Employee {
        employee.employeeID = null
        var empl = Gson().toJson(employee)
        var part: MultipartBody.Part? = if(photo == null) {
            null
        } else {
            MultipartBody.Part.createFormData("photo", photo?.name, RequestBody.create(MediaType.parse("image/jpeg"), photo))
        }
        return service.createEmployee(RequestBody.create(MediaType.parse("text/plain"), empl), part)
    }
}