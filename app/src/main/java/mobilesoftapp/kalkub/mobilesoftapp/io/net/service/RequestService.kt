package mylaw.app.io.net.service

import com.google.gson.GsonBuilder
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService
import mobilesoftapp.kalkub.mobilesoftapp.BuildConfig
import org.androidannotations.annotations.EService
import retrofit.RestAdapter
import retrofit.converter.Converter
import retrofit.converter.GsonConverter

@EService
open class RequestService : RetrofitGsonSpiceService() {

    override fun getServerUrl(): String {
        return APIData.BASE_URL
    }

    object APIData {
        const val BASE_URL = "http://testapp.mobilesoft.cz/api"
        const val BASE_IMAGE = "http://testapp.mobilesoft.cz"
    }

    override fun createRestAdapterBuilder(): RestAdapter.Builder? {
        val builder = super.createRestAdapterBuilder()
        builder.setLogLevel(if (BuildConfig.DEBUG) RestAdapter.LogLevel.FULL else RestAdapter.LogLevel.NONE)
        return builder
    }

    override fun onCreate() {
        super.onCreate()
        addRetrofitInterface(ApiService::class.java)
    }

    override fun createConverter(): Converter? {
        return GsonConverter(GsonBuilder()
                .setDateFormat("yyyy-MM-dd")
                .create()
        )
    }

}