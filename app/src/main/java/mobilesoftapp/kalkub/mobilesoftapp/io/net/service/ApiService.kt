package mylaw.app.io.net.service

import mobilesoftapp.kalkub.mobilesoftapp.io.net.model.Employee
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit.http.*

interface ApiService {

    @GET("/employees")
    fun getEmployees() : ArrayList<Employee>

    @GET("/employees/{id}")
    fun getEmployee(@Path("id") id: Long) : Employee

    @Multipart
    @POST("/employees")
    fun createEmployee(@Part("json") json: RequestBody,
                       @Part("photo") photo: MultipartBody.Part?) : Employee

}